import ignite from 'gulp-ignite';
import browserify from 'gulp-ignite-browserify';
import sass from 'gulp-ignite-sass';
import mocha from 'gulp-ignite-mocha';
import sassLint from 'gulp-ignite-sass-lint';
import eslint from 'gulp-ignite-eslint';
import istanbul from 'gulp-ignite-istanbul';
import babelify from 'babelify';

const SRC_PATH = './src';
const DEMO_PATH = './demo';

const dev = {
  name: 'dev',
  description: 'Run during development. [\'browserify\', \'sass\']',
  run: ['browserify', 'sass'],
};

const tasks = [
  browserify,
  sass,
  mocha,
  sassLint,
  eslint,
  istanbul,
  dev,
];

const options = {
  browserify: {
    options: { transform: [babelify] },
    src: `${DEMO_PATH}/src/demo.js`,
    dest: DEMO_PATH,
    filename: 'demo.js',
    watchFiles: [
      `${SRC_PATH}/**/*.js`,
      `${DEMO_PATH}/src/*.js`,
    ],
  },
  sass: {
    src: `${DEMO_PATH}/src/demo.scss`,
    dest: DEMO_PATH,
    watchFiles: [
      `${SRC_PATH}/**/*.scss`,
      `${DEMO_PATH}/src/*.scss`,
    ],
  },
  eslint: {
    src: `${SRC_PATH}/**/*.js`,
  },
  sassLint: {
    src: `${SRC_PATH}/**/*.scss`,
    configFile: './.sass-lint.yml',
  },
  mocha: {
    src: `${SRC_PATH}/*.spec.js`,
    watchFiles: `${SRC_PATH}/*.js`,
    options: {
      compilers: 'js:babel-core/register',
    },
  },
  istanbul: {
    src: [
      `${SRC_PATH}/*.js`,
      `!${SRC_PATH}/*.spec.js`,
    ],
    testTask: 'mocha',
  },
};

ignite.start(tasks, options);
