import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import ReactSimpleCarousel from '../../src';

const Image = ({ active, src, alt, onClick }) => (
  <div className={`image ${active ? 'image--active' : ''}`}>
    <img src={src} alt={alt} onClick={onClick} />
  </div>
);

Image.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  active: PropTypes.bool,
  onClick: PropTypes.func,
};

class Carousel extends Component {
  constructor(props) {
    super(props);

    this.handleResize = this.handleResize.bind(this);
    this.onDetailCarouselChange = this.onDetailCarouselChange.bind(this);
    this.onThumbClick = this.onThumbClick.bind(this);

    this.state = {
      slidesToShow: this.getSlidesToShow(),
      activeDetailIdx: 1,
    };
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleResize);
  }

  onDetailCarouselChange(prev, current) {
    if (this.thumbs) {
      this.setState({
        activeDetailIdx: current,
      });

      this.thumbs.goToIndex(current);
    }
  }

  onThumbClick(idx) {
    if (this.carousel) {
      this.carousel.goToIndex(idx);
    }
  }

  getSlidesToShow() {
    if (window.innerWidth > 800) {
      return 4;
    }

    if (window.innerWidth > 600) {
      return 3;
    }

    if (window.innerWidth > 400) {
      return 2;
    }

    return null;
  }

  handleResize() {
    this.setState({
      slidesToShow: this.getSlidesToShow(),
    });
  }

  render() {
    const items = [
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
      'http://placehold.it/1200x650',
    ];

    const arrows = false;
    const pagination = false;

    return (
      <div>
        <div className="carousel">
          <ReactSimpleCarousel
            slidesToShow={1}
            arrows={arrows}
            pagination={pagination}
            onSlideChange={this.onDetailCarouselChange}
            ref={(ref) => { this.carousel = ref; }}
          >
            {
              items.map((item, idx) => (
                <Image key={idx} src={`${item}?text=Slide+${idx + 1}`} alt="slide" />
              ))
            }
          </ReactSimpleCarousel>
        </div>

        {
          this.state.slidesToShow &&
            <div className="carousel2">
              <ReactSimpleCarousel
                slidesToShow={this.state.slidesToShow}
                slidesToScroll={this.state.slidesToShow}
                pagination={false}
                onSlideSelect={this.onThumbClick}
                ref={(ref) => { this.thumbs = ref; }}
              >
                {
                  items.map((item, idx) => (
                    <Image
                      key={idx}
                      src={`${item}?text=Slide+${idx + 1}`}
                      alt="slide"
                      active={this.state.activeDetailIdx === idx}
                    />
                  ))
                }
              </ReactSimpleCarousel>
            </div>
        }
      </div>
    );
  }
}

ReactDOM.render(<Carousel />, document.getElementById('carousel'));
