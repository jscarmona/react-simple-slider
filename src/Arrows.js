import React, { PropTypes } from 'react';

const propTypes = {
  goToNextSlide: PropTypes.func.isRequired,
  goToPreviousSlide: PropTypes.func.isRequired,
  hasNextSlide: PropTypes.bool,
  hasPreviousSlide: PropTypes.bool,
};

const defaultProps = {
  hasNextSlide: true,
  hasPreviousSlide: true,
};

const Arrow = ({
  hasNextSlide,
  hasPreviousSlide,
  goToNextSlide,
  goToPreviousSlide,
}) => (
  <div className="slider__arrows">
    <button
      className={`slider__arrow slider__arrow--previous${!hasPreviousSlide ? ' slider__arrow--disabled' : ''}`}
      onClick={goToPreviousSlide}
      children="Previous"
    />
    <button
      className={`slider__arrow slider__arrow--next${!hasNextSlide ? ' slider__arrow--disabled' : ''}`}
      onClick={goToNextSlide}
      children="Next"
    />
  </div>
);

Arrow.propTypes = propTypes;
Arrow.defaultProps = defaultProps;

export default Arrow;
