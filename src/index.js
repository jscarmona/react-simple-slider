import React, { Component, Children, PropTypes } from 'react';
import Swipe from 'react-easy-swipe';
import Arrows from './Arrows';
import Pagination from './Pagination';

class Slider extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.array,
      PropTypes.object,
    ]),
    slidesToShow: PropTypes.number,
    slidesToScroll: PropTypes.number,
    arrows: PropTypes.bool,
    pagination: PropTypes.bool,
    onSlideChange: PropTypes.func,
    onSlideSelect: PropTypes.func,
  }

  static defaultProps = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    pagination: true,
  }

  constructor(props) {
    super(props);

    this.handleResize = this.handleResize.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.goToNextSlide = this.goToNextSlide.bind(this);
    this.goToPreviousSlide = this.goToPreviousSlide.bind(this);

    this.state = {
      currentIndex: 0,
      sliderWidth: 0,
      animate: true,
    };
  }

  componentDidMount() {
    window.setTimeout(this.handleResize, 500);
    window.addEventListener('resize', this.handleResize);
  }

  componentDidUpdate(prevProps, prevState) {
    const { onSlideChange } = this.props;

    if (typeof onSlideChange === 'function') {
      onSlideChange(prevState.currentIndex, this.state.currentIndex);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  getSlideWidth() {
    return this.state.sliderWidth / this.props.slidesToShow;
  }

  getSliderTrackX(index) {
    const { children, slidesToScroll } = this.props;
    const maxIndex = children.length - slidesToScroll;
    const nextIndex = (index < maxIndex) ? index : maxIndex;

    return this.getSlideWidth() * nextIndex;
  }

  hasPreviousSlide() {
    return this.state.currentIndex > 0;
  }

  hasNextSlide() {
    const { children, slidesToScroll } = this.props;

    return this.state.currentIndex < (children.length - slidesToScroll);
  }

  goToPreviousSlide() {
    this.goToIndex(this.state.currentIndex - this.props.slidesToScroll);
  }

  goToNextSlide() {
    this.goToIndex(this.state.currentIndex + this.props.slidesToScroll);
  }

  goToIndex(index) {
    const { children, slidesToShow } = this.props;
    let nextIndex = index;

    if (nextIndex < 0) {
      nextIndex = 0;
    }

    if ((nextIndex + 1) > (children.length - slidesToShow)) {
      nextIndex = children.length - slidesToShow;
    }

    this.setState({
      animate: true,
      currentIndex: nextIndex,
    });
  }

  handleResize() {
    if (this.slider) {
      this.setState({
        animate: false,
        sliderWidth: this.slider.offsetWidth,
      });
    }
  }

  renderSlides() {
    const { children, onSlideSelect, slidesToScroll } = this.props;
    const { currentIndex } = this.state;

    return Children.map(children, (item, idx) => {
      const props = {
        key: item.key || `${idx}`,
        style: {
          width: this.getSlideWidth(),
        },
        className: 'slider__slide',
      };

      if (typeof onSlideSelect === 'function') {
        props.onClick = () => { onSlideSelect(idx); };
      }

      if (currentIndex === idx) {
        props.className = `${props.className} slider__slide--current`;
      }

      if (idx >= currentIndex && idx < currentIndex + slidesToScroll) {
        props.className = `${props.className} slider__slide--active`;
      }

      return (
        <div {...props} children={item} />
      );
    });
  }

  render() {
    const { arrows, children, pagination } = this.props;
    const { currentIndex, sliderWidth, animate } = this.state;
    const trackStyle = {
      width: sliderWidth * children.length,
      transform: `translate3d(-${this.getSliderTrackX(currentIndex)}px, 0px, 0px)`,
    };

    if (animate) {
      trackStyle.transition = 'transform 0.2s';
    }

    return (
      <div className="slider" ref={ref => { this.slider = ref; }}>
        <div className="slider__container">
          <Swipe
            className="slider__track"
            style={trackStyle}
            onSwipeLeft={this.goToNextSlide}
            onSwipeRight={this.goToPreviousSlide}
          >
            {this.renderSlides()}
          </Swipe>
        </div>
        {
          arrows &&
            <Arrows
              hasPreviousSlide={this.hasPreviousSlide()}
              hasNextSlide={this.hasNextSlide()}
              goToNextSlide={this.goToNextSlide}
              goToPreviousSlide={this.goToPreviousSlide}
            />
        }
        {
          pagination &&
            <Pagination
              currentIndex={currentIndex}
              count={children.length}
              goToIndex={this.goToIndex}
            />
        }
      </div>
    );
  }
}

export default Slider;
