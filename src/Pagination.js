import React, { PropTypes } from 'react';

const propTypes = {
  goToIndex: PropTypes.func.isRequired,
  currentIndex: PropTypes.number.isRequired,
  count: PropTypes.number.isRequired,
};

const Pagination = ({ currentIndex, goToIndex, count }) => (
  <div className="slider__pagination">
    <ol className="slider__dots">
      {Array(count).fill().map((_, idx) => (
        <li
          key={idx}
          className={`slider__dot${currentIndex === idx ? ' slider__dot--active' : ''}`}
          onClick={() => goToIndex(idx)}
        />
      ))}
    </ol>
  </div>
);

Pagination.propTypes = propTypes;

export default Pagination;
